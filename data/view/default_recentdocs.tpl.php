<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>

<div id="zjgx" class="columns zjgx o-v <?php echo $data['config']['style']?>" bid="<?php echo $bid?>">
    <h2 class="col-h2">最近更新</h2>
    <a href="index.php?list-recentchange" class="more">更多>></a>
    <ul class="col-ul font-14 ">
       <?php foreach((array)$data['doclist'] as $doc) {?>
            <li><a href="index.php?doc-view-<?php echo $doc['did']?>"  class="ctm" title="<?php echo $doc['title']?>"><?php echo $doc['shorttitle']?></a><span><?php echo $doc['lastedit']?></span></li>
       <?php } ?>
    </ul>
</div>