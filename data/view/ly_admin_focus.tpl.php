<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<?php include $this->gettpl('admin_header');?>
<script type="text/javascript">
var searchdata = "<?php echo $searchdata?>".replace(/-/g, ",");
$(document).ready(function() {
	$(".dialog_show").click(function(){
		var src = $(this).attr("src");
		var img_str = '<img src="'+src+'">';
		var title = $(this).attr("title");
		$.dialog.box('dialog_show', title, img_str);
	});
	$(".eidt_show").click(function(){
		var src = $(this).attr("src");
		var title = $(this).attr("title");
		$.dialog({
			id:'eidt_show',
			position:'center',
			width:500,
			height:450,
			title:title,
			type:'iframe',
			url:src,
			styleOverlay:{backgroundColor:'#ffffff'}
	  	});
	});	
	
	$(".dialog_update").click(function(){
		var did = $(this).attr('did');
		window.location.href="index.php?admin_focus-updateimg-"+did+'-'+searchdata;
	})
});	

	function updateimg(){
		if($("input[name='did[]']:checked").length==0){
			$.dialog.box('immageshow', '注意', '请选择词条!');
			return false;
		} else if(confirm('确定要更新图片地址么？')){
			$("form[name='focusdoc']").attr('action',"index.php?admin_focus-updateimg-"+searchdata);
			$("form[name='focusdoc']").submit();
		}else{
			return false;
		}
	}
	
	function selectALL(obj){
		$(".box").attr("checked",obj.checked);
	}
	function checkdel(){
		if($("input[name='did[]']:checked").length==0){
		$.dialog.box('immageshow', '注意', '请选择词条!');
		return false;
		}else if(confirm('确认删除？')){
			$("form[name='focusdoc']").attr('action','index.php?admin_focus-remove');
			$("form[name='focusdoc']").submit();
		}else{
			return false;
		}
	}
	function updateorder(){
		if($("input[name='did[]']:checked").length==0){
		$.dialog.box('immageshow', '注意', '请选择词条!');
		return false;
		}else if(confirm('确定要修改排列顺序吗？')){
			$("form[name='focusdoc']").attr('action','index.php?admin_focus-reorder');
			$("form[name='focusdoc']").submit();
		}else{
			return false;
		}
	}
	function checklen(obj){
		num=obj.value;
		if(num!=''){
			if(num>127){
				alert('支持的最大数字为127.');
				obj.value=127;
			}
			if(isNaN(num)==true){
				alert("请输入数字!");
				obj.value=0;
			}
		}else {
			obj.value=0;
		}	
	}
</script>
<p class="map">内容管理：词条管理</p>
<p class="sec_nav">词条管理：
<a href="index.php?admin_doc" > <span>管理词条</span></a>
<a href="index.php?admin_focus-focuslist" class="on"  ><span>推荐词条</span></a>
<a href="index.php?admin_synonym" ><span>管理同义词</span></a>
<a href="index.php?admin_relation" ><span>相关词条</span></a>
<a href="index.php?admin_edition" ><span>版本评审</span></a>
<a href="index.php?admin_cooperate" ><span>待完善词条</span></a> 
<a href="index.php?admin_nav" class="new"><span>导航模块<label class="red">new</label></span></a> 
</p>
<h3 class="col-h3">推荐词条</h3>
<h3 class="tol_table">[ 共 <b><?php echo $docsum?></b> 条词条 ]</h3>
<div class="synonym">
	<form name="focusdoc" id="focusdoc"  method="POST">
		<table class="table">
			<thead>
				<tr>
					<td style="width:50px;">选择</td>
					<td style="width:80px;">显示顺序</td>
					<td style="width:240px;">标题</td>
					<td style="width:80px;">词条类别</td>
					<td style="width:180px;">图片地址（点击查看）</td>
					<td>编辑</td>
				</tr>
			</thead>
			<?php foreach((array)$lists as $list) {?>
			<tr>
				<td><input type="hidden" name="all_focus_did[]" value = "<?php echo $list['did']?>" />
					<input type="checkbox" class="box" name="did[]" value="<?php echo $list['did']?>" /></td>
				<td><input type="text" name="order<?php echo $list['did']?>" value="<?php echo $list['displayorder']?>" size="2" maxlength="3" onblur="checklen(this)"  /></td>
				<td><a href='index.php?doc-view-<?php echo $list['did']?>' target='_blank'><?php echo $list['title']?></a></td>
				<td> <?php if($list['type'] == 1) { ?><font color="Red">推荐词条</font><?php } elseif($list['type'] == 2) { ?><font color="blue">热门词条</font><?php } elseif($list['type'] == 3) { ?><font color="green">精彩词条</font><?php } else { ?>无<?php } ?> </td>
				<td><?php if($list['image']=='') { ?>
					无
					<?php } else { ?>
					<a href="javascript:void(0)" src="<?php echo $list['image']?>" class="dialog_show" title='查看图片'>点击查看</a>
					<?php } ?></td>
				<td><a href="javascript:void(0)" class="eidt_show" src="index.php?admin_focus-edit-<?php echo $list['did']?>" class="dialog_show">编辑</a></td>
			</tr>
			<?php } ?>
			<tr>
				<td colspan="6">
					<label>
						<input name="chkall" class="box" onclick="selectALL(this);"  type="checkbox" />&nbsp;&nbsp;全选
					</label>
					<input type="button" class="inp_btn2 m-r10"  value="删除选中的词条" onclick="checkdel();"/>
					<input type="button" class="inp_btn2 m-r10"  value="修改推荐顺序" onclick="updateorder();"/>
					<input type="button" class="inp_btn2" value="更新选中词条图片地址" onclick="updateimg()"  />
				 </td>
			</tr>
			<tr>
				<td colspan="6"><p class="fenye a-r"> <?php echo $departstr?> </p></td>
			</tr>
		</table>
	</form>
</div>
<?php include $this->gettpl('admin_footer');?> 