<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<?php include $this->gettpl('admin_header');?>
<script type="text/javascript">
function doSubmit(){
	var sandbox_id = $.trim($("input[name*='sandbox_id']").val());
	if (sandbox_id && !/\d+/.test(sandbox_id)){
		alert('“设置指定编辑实验词条的ID”必须为数字!');
		$("input[name*='sandbox_id']").focus();
		return false;
	}
	var max_newdocs = parseInt($.trim($("input[name*='max_newdocs']").val()));
	if(max_newdocs > 100 || max_newdocs < 0 ) {
		alert('首次编辑审核数范围错误。');
		$("input[name*='max_newdocs']").focus();
		return false;
	}
}
</script>
<p class="map">全局：内容设置</p>
<p class="sec_nav">内容设置： <a href="index.php?admin_setting-index"> <span>首页设置</span></a> <a href="index.php?admin_setting-listdisplay" ><span>列表设置</span></a> <a href="index.php?admin_setting-watermark" ><span>图片设置</span></a> <a href="index.php?admin_setting-docset" class="on"><span>词条设置</span></a> <a href="index.php?admin_setting-search" ><span>搜索设置</span></a> <a href="index.php?admin_setting-attachment" ><span>附件设置</span></a></p>
<h3 class="col-h3">词条设置</h3>
<form method="POST" action="index.php?admin_setting-docset" onsubmit="return doSubmit();">
<table class="table">
	<tr>
		<td width="500px"><span>参数名称</span></td>
		<td><span>参数值</span></td>
	</tr>
	<tr>
		
		<td><span>指定编辑实验词条的ID</span>默认为最后一个词条，输入指定词条的ID，将改变默认值</td>
		<td>
			<input type="input" class="inp_txt" name="setting[sandbox_id]" value="<?php echo $basecfginfo['sandbox_id']?>"/>
		</td>
	</tr>
	<tr>
		<td><span>审核词条</span>创建或编辑过的词条经过审核后才能在前台显示</td>
		<td>
			<label><input <?php if($basecfginfo['verify_doc'] == 1 ) { ?>checked<?php } ?> type="radio" name="setting[verify_doc]" value="1" onclick="$('#first_edit').hide();" />是</label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input <?php if($basecfginfo['verify_doc'] == -1 ) { ?>checked<?php } ?> type="radio" name="setting[verify_doc]" value="-1" onclick="$('#first_edit').show();" />仅首次编辑审核</label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input <?php if(!$basecfginfo['verify_doc'] ) { ?>checked<?php } ?> type="radio" name="setting[verify_doc]" value="0" onclick="$('#first_edit').hide();" />否</label>
		</td>
	</tr>
	<tr id="first_edit" <?php if($basecfginfo['verify_doc'] != -1 ) { ?>style="display:none;"<?php } ?>>
		<td><span>首次编辑审核</span>开启新注册用户第一次编辑审核，新注册用户在审核前最大编辑的词条数</td>
		<td>
			<input type="input" class="inp_txt" style="width: 30px;" maxlength="3" name="setting[max_newdocs]" value="<?php if(isset($basecfginfo['max_newdocs'])) { ?><?php echo $basecfginfo['max_newdocs']?><?php } else { ?>5<?php } ?>"/>
			0 表示不限制，100 为最大上限
		</td>
	</tr>
	<tr>
		<td><span>是否允许以匿名形式发表评论</span>本项设置如果开启，有权限发表评论的用户可以选择以匿名用户的身份发表评论。</td>
		<td>
			<label><input type="radio"  name="setting[comments]" value="1" <?php if($basecfginfo['comments']=='1') { ?>checked<?php } ?>/>是</label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio"  name="setting[comments]" value="0" <?php if($basecfginfo['comments']=='0') { ?>checked<?php } ?>/>否</label>
		</td>
	</tr>
	<tr>
		<td><span>是否在编辑器中过滤外部链接</span>编辑器中自动过滤外部链接可以有效减少灌水和广告。</td>
		<td>
			<label><input type="radio"  name="setting[filter_external]" value="1" <?php if($basecfginfo['filter_external']==='1') { ?>checked<?php } ?>/>是</label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input type="radio"  name="setting[filter_external]" value="0" <?php if($basecfginfo['filter_external']==='0'||is_null($basecfginfo['filter_external'])) { ?>checked<?php } ?>/>否</label>
		</td>
	</tr>
	<tr>
		<td><span>新创建及最新编辑词条是否保存为历史版本</span>若选择"是"，则创建词条的版本和最新的词条版本都保存在历史版本中。若选择"否"，则不保存当前最新版本。</td>
		<td>
			<label>
			<input type="radio"  name="setting[base_createdoc]" value="1" <?php if($basecfginfo['base_createdoc']=='1') { ?>checked<?php } ?>/>是</label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio"  name="setting[base_createdoc]" value="0" <?php if($basecfginfo['base_createdoc']=='0') { ?>checked<?php } ?>/>否</label>
		</td>
	</tr>
	<tr>
		<td><span>是否开启参考资料</span>开启参考资料，可以为词条参加参考资料，否则则不可以</td>
		<td>
			<label>
			<input type="radio"  name="setting[base_isreferences]" value="1" <?php if($basecfginfo['base_isreferences']=='1') { ?>checked<?php } ?>/>是</label>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio"  name="setting[base_isreferences]" value="0" <?php if($basecfginfo['base_isreferences']=='0') { ?>checked<?php } ?>/>否</label>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input class="inp_btn" name="docsubmit" type="submit" value="保 存" />&nbsp;&nbsp;
		</td>
	</tr>
</table>
</from>
<?php include $this->gettpl('admin_footer');?>