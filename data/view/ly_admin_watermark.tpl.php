<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<?php include $this->gettpl('admin_header');?>
<script type="text/javascript" src="js/pickcolor.js"></script>
<script type="text/JavaScript">
function textareasize(obj) {
	if(obj.scrollHeight > 90) {
		obj.style.height = obj.scrollHeight + 'px';
	}
}
function doSubmit(){
	var img_width_small=$.trim($('#img_width_small').val());
	img_width_small = parseInt(img_width_small);
	if(isNaN(img_width_small)==true){
		alert('您输入的词条小图尺寸【宽】不是一个有效数字！');
		$('#img_width_small').focus();
		return false;
	}
	var img_height_small=$.trim($('#img_height_small').val());
	img_height_small = parseInt(img_height_small);
	if(isNaN(img_height_small)==true){
		alert('您输入的词条小图尺寸【高】不是一个有效数字！');
		$('#img_height_small').focus();
		return false;
	}
	var img_width_big=$.trim($('#img_width_big').val());
	img_width_big = parseInt(img_width_big);
	if(isNaN(img_width_big)==true){
		alert('您输入的词条大图尺寸【宽】不是一个有效数字！');
		$('#img_width_big').focus();
		return false;
	}
	var img_height_big=$.trim($('#img_height_big').val());
	img_height_big = parseInt(img_height_big);
	if(isNaN(img_height_big)==true){
		alert('您输入的词条大图尺寸【高】不是一个有效数字！');
		$('#img_height_big').focus();
		return false;
	}
	if(confirm('警告：提交后将覆盖系统当前参数设置，是否继续?')==false){
		return false;
	}
}
</script>
<p class="map">全局：内容设置</p>
<p class="sec_nav">内容设置： <a href="index.php?admin_setting-index"> <span>首页设置</span></a> <a href="index.php?admin_setting-listdisplay"><span>列表设置</span></a> <a href="index.php?admin_setting-watermark" class="on"><span>图片设置</span></a> <a href="index.php?admin_setting-docset" ><span>词条设置</span></a> <a href="index.php?admin_setting-search" ><span>搜索设置</span></a> <a href="index.php?admin_setting-attachment" ><span>附件设置</span></a></p>
<h3 class="col-h3">图片设置</h3>
<form id="waterform" method="POST" action="index.php?admin_setting-watermark" onsubmit="return doSubmit();">
<table class="table">
		<tr>
			<td style="width:600px"><span>图片本地化</span>开启后，词条中的网上图片在<font color=#ff0000>编辑过后</font>将自动保存到本地，是否开启？</td>
			<td >
				<label><input type="radio" name="setting[auto_picture]" value="1" <?php if($basecfginfo['auto_picture']=='1') { ?>checked<?php } ?>/>是</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio"  name="setting[auto_picture]" value="0" <?php if($basecfginfo['auto_picture']=='0') { ?>checked<?php } ?>/>否</label>
			</td>
		</tr>
		<tr>
			<td><span>词条大图尺寸</span>建议在300～600之间，默认是300*300</td>
			<td>
				宽 <input type="input" class="inp_txt2" id="img_width_big" name="setting[img_width_big]" value="<?php echo $basecfginfo['img_width_big']?>" size="5" maxlength="3"/> *
				高 <input type="input" class="inp_txt2" id="img_height_big" name="setting[img_height_big]" value="<?php echo $basecfginfo['img_height_big']?>" size="5" maxlength="3"/>
				像素
			</td>
		</tr>
		<tr>
			<td ><span>词条小图尺寸</span>建议在100～200之间，默认是140*140</td>
			<td>
				宽 <input type="input" class="inp_txt2" id="img_width_small" name="setting[img_width_small]" value="<?php echo $basecfginfo['img_width_small']?>" size="5" maxlength="3"/> *
				高 <input type="input" class="inp_txt2" id="img_height_small" name="setting[img_height_small]" value="<?php echo $basecfginfo['img_height_small']?>" size="5" maxlength="3"/>
				像素
			</td>
		</tr>
	<tr>
		<td><span>图片处理库类型:</span>
			GD 是最广泛的处理库但是使用的系统资源较多。ImageMagick 速度快系统资源占用少，但需要服务器有执行命令行命令的权限。如果您的服务器有条件安装此程序，请到 <a href="http://www.imagemagick.org" target="_blank">http://www.imagemagick.org</a> 下载，安装后在下面指定安装的路径
		</td>
		<td>
			<ul>
				<li><input name="settingsnew[imagelib]" value="0" <?php if($settingsnew['imagelib']=="0") { ?>checked="checked"<?php } ?> onclick="$('#imagelibext').attr('style','display:none');" type="radio"> GD</li>
				<li><input name="settingsnew[imagelib]" value="1" <?php if($settingsnew['imagelib']=="1") { ?>checked="checked"<?php } ?> onclick="$('#imagelibext').attr('style','display:');" type="radio"> ImageMagick</li>
			</ul>
		</td>
	</tr>
	<tr id="imagelibext" <?php if($settingsnew['imagelib']!=="1") { ?>style="display:none"<?php } ?>>
		<td><span>ImageMagick 程序安装路径:</span>ImageMagick 6 程序的安装路径。如果此处路径填写错误，程序将尝试调用GD库。</td>
		<td ><input name="settingsnew[imageimpath]" value="<?php echo $settingsnew['imageimpath']?>" class="inp_txt" type="text"></td>
	</tr>
	<tr>
	<td><span>水印:</span>您可以设置自动为用户上传的 JPG/PNG/GIF 图片附件添加水印，请在此选择水印添加的位置(3x3 共 9 个位置可选)。不支持动画 GIF 格式</td>
	<td >
		<table>
				<tr>
					<td colspan="3"><input name="settingsnew[watermarkstatus]" value="0" <?php if($settingsnew['watermarkstatus']=="0") { ?>checked="checked"<?php } ?> type="radio">不启用水印功能</td>
				</tr>
				<tr>
					<td><input name="settingsnew[watermarkstatus]" value="1" <?php if($settingsnew['watermarkstatus']=="1") { ?>checked="checked"<?php } ?> type="radio"> 顶部左</td>
					<td><input name="settingsnew[watermarkstatus]" value="2" <?php if($settingsnew['watermarkstatus']=="2") { ?>checked="checked"<?php } ?> type="radio"> 顶部中</td>
					<td><input name="settingsnew[watermarkstatus]" value="3" <?php if($settingsnew['watermarkstatus']=="3") { ?>checked="checked"<?php } ?> type="radio"> 顶部右</td>
				</tr>
				<tr>
					<td><input name="settingsnew[watermarkstatus]" value="4" <?php if($settingsnew['watermarkstatus']=="4") { ?>checked="checked"<?php } ?> type="radio"> 中部左</td>
					<td><input name="settingsnew[watermarkstatus]" value="5" <?php if($settingsnew['watermarkstatus']=="5") { ?>checked="checked"<?php } ?> type="radio"> 中部中</td>
					<td><input name="settingsnew[watermarkstatus]" value="6" <?php if($settingsnew['watermarkstatus']=="6") { ?>checked="checked"<?php } ?> type="radio"> 中部右</td>
				</tr>
				<tr>
					<td><input name="settingsnew[watermarkstatus]" value="7" <?php if($settingsnew['watermarkstatus']=="7") { ?>checked="checked"<?php } ?> type="radio"> 底部左</td>
					<td><input name="settingsnew[watermarkstatus]" value="8" <?php if($settingsnew['watermarkstatus']=="8") { ?>checked="checked"<?php } ?> type="radio"> 底部中</td>
					<td><input name="settingsnew[watermarkstatus]" value="9" <?php if($settingsnew['watermarkstatus']=="9") { ?>checked="checked"<?php } ?> type="radio"> 底部右</td>
				</tr>
		</table>
	</td>
	</tr>
	<tr >
		<td><span>水印添加条件:</span>小于此尺寸的图片附件将不添加水印</td>
		<td ><input name="settingsnew[watermarkminwidth]" value="<?php echo $settingsnew['watermarkminwidth']?>" class="inp_txt2" size="8" type="text"> X <input name="settingsnew[watermarkminheight]" value="<?php echo $settingsnew['watermarkminheight']?>" class="inp_txt2" size="8" type="text"></td>
	</tr>
	<tr>
		<td><span>水印图片类型:</span>水印图片保存在./style/default/watermark/logo.(gif|png)，请手动替换相应格式的文件，以实现不同效果的水印。</td>
		<td >
			<ul>
				<li><input name="settingsnew[watermarktype]" value="0" <?php if($settingsnew['watermarktype']=="0") { ?>checked="checked"<?php } ?>  onclick="$('#watermarktypeext').attr('style','display:none');" type="radio" checked="checked"> GIF 图片水印</li>
				<li><input name="settingsnew[watermarktype]" value="1" <?php if($settingsnew['watermarktype']=="1") { ?>checked="checked"<?php } ?>  onclick="$('#watermarktypeext').attr('style','display:none');" type="radio"> PNG 图片水印</li>
				<li><input name="settingsnew[watermarktype]" value="2" <?php if($settingsnew['watermarktype']=="2") { ?>checked="checked"<?php } ?>  onclick="$('#watermarktypeext').attr('style','display:');" type="radio"> 文本类型水印</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td><span>水印融合度:</span>设置 GIF 类型水印图片与原始图片的融合度，范围为 1～100 的整数，数值越大水印图片透明度越低。PNG 类型水印本身具有真彩透明效果，无须此设置。本功能需要开启水印功能后才有效。</td>
		<td ><input class="inp_txt2" size="8" name="settingsnew[watermarktrans]" <?php if($settingsnew['watermarktrans'] !=="0") { ?>value="<?php echo $settingsnew['watermarktrans']?>" <?php } else { ?> value="65" <?php } ?>  type="text"></td>
	</tr>
	
	<tr>
		<td><span>JPEG 水印质量:</span>设置 JPEG 类型的图片附件添加水印后的质量参数，范围为 0～100 的整数，数值越大结果图片效果越好，但尺寸也越大。本功能需要开启水印功能后才有效:</td>
		<td ><input class="inp_txt2" size="8" name="settingsnew[watermarkquality]" <?php if($settingsnew['watermarkquality'] !=="0") { ?>value="<?php echo $settingsnew['watermarkquality']?>" <?php } else { ?> value="80" <?php } ?>  type="text"></td>
	</tr>

	<tbody id="watermarktypeext" <?php if($settingsnew['watermarktype']!=="2") { ?>style="display:none"<?php } ?>>
	<tr>
		<td><span>文本水印文字:</span>如果您指定的 TrueType 字体为中文字体文件，那么您可以在文本水印中书写中文。<br>注意：默认提供的ant2.ttf不支持中文，如果需要使用中文文本水印，需要添加中文字体文件。</td>
		<td ><textarea class="textarea" rows="6" onkeyup="textareasize(this)" name="settingsnew[watermarktext][text]" id="settingsnew[watermarktext][text]" cols="50" ><?php echo $settingsnew['watermarktext'][text]?></textarea></td>
	</tr>
	<tr>
		<td><span>文本水印 TrueType 字体文件名:</span>可以将您的字体文件存放到./style/default/ 目录下。如使用中文 TTF 字体请使用包含完整中文汉字的字体文件。</td>
		<td>
			<select name="settingsnew[watermarktext][fontpath]">
			<?php foreach((array)$ttf as $value) {?>
				<option value="<?php echo $value?>" <?php if($value==$settingsnew['watermarktext'][fontpath]) { ?> selected <?php } ?> ><?php echo $value?></option>
			<?php } ?>
			</select>
		</td>
	</tr>
	<tr>
		<td><span>文本水印字体大小:</span>设置文本水印字体大小，请按照字体设置相应的大小，建议在10～36之间。</td>
		<td ><input class="inp_txt2" size="8" name="settingsnew[watermarktext][size]" value="<?php echo $settingsnew['watermarktext'][size]?>"  type="text"></td>
	</tr>
	<tr>
		<td><b>文本水印显示角度:</b><br/>0 度为水平从左向右阅读文本</td>
		<td ><input class="inp_txt2" size="8" name="settingsnew[watermarktext][angle]" value="<?php echo $settingsnew['watermarktext'][angle]?>"  type="text"></td>
	</tr>
	</tr>
	<tr>
		<td><span>文本水印字体颜色:</span>输入 16 进制颜色代表文本水印字体颜色</td>
		<td >
			<input class="inp_txt2" size="8" name="settingsnew[watermarktext][color]" id="left_framcolor" maxlength="7" value="<?php echo $settingsnew['watermarktext'][color]?>" type="text" class="inp_txt wid90" onclick="PickColor.init(this)" onkeyup="PickColor.keyup(this.value)" onchange="PickColor.change(this.value)" />
			<input type="button" onclick="PickColor.init(this)" style="background-color:<?php echo $settingsnew['watermarktext'][color]?>" class="choose_c"/>
		</td>
	</tr>
	<tr>
		<td><span>文本水印阴影横向偏移量:</span>设置文本水印阴影横向偏移量，此数值不宜设置的太大</td>
		<td ><input class="inp_txt2" size="8" name="settingsnew[watermarktext][shadowx]" value="<?php echo $settingsnew['watermarktext'][shadowx]?>" type="text"></td>
	</tr>
	<tr>
		<td><span>文本水印阴影纵向偏移量:</span>设置文本水印阴影纵向偏移量，此数值不宜设置的太大</td>
		<td ><input class="inp_txt2" size="8" name="settingsnew[watermarktext][shadowy]" value="<?php echo $settingsnew['watermarktext'][shadowy]?>"  type="text"></td>
	</tr>
	<tr>
		<td><span>文本水印阴影颜色:</span>输入 16 进制颜色代表文本水印阴影字体颜色</td>
		<td>
			<input class="inp_txt2" size="8" name="settingsnew[watermarktext][shadowcolor]" id="leftitle_bgcolor" maxlength="7" value="<?php echo $settingsnew['watermarktext'][shadowcolor]?>" type="text" class="inp_txt wid90" onclick="PickColor.init(this)" onkeyup="PickColor.keyup(this.value)" onchange="PickColor.change(this.value)" />
			<input type="button" onclick="PickColor.init(this)" style="background-color:<?php echo $settingsnew['watermarktext'][shadowcolor]?>" class="choose_c"/></td>
	</tr>
	<tr>
		<td><span>文本水印横向偏移量(ImageMagick):</span>设置水印文本输出后向屏幕中央的横向的偏移值。本设置只适用于 ImageMagick 图片处理库</td>
		<td ><input class="inp_txt2" size="8" name="settingsnew[watermarktext][translatex]" value="<?php echo $settingsnew['watermarktext'][translatex]?>"  type="text"></td>
	</tr>
	<tr>
		<td><span>文本水印纵向偏移量(ImageMagick):</span>设置水印文本输出后向屏幕中央的纵向的偏移值。本设置只适用于 ImageMagick 图片处理库</td>
		<td ><input class="inp_txt2" size="8" name="settingsnew[watermarktext][translatey]" value="<?php echo $settingsnew['watermarktext'][translatey]?>"  type="text"></td>
	</tr>
	<tr>
		<td ><span>文本水印横向倾斜角度(ImageMagick):</span>设置水印文本横向的倾斜角度。本设置只适用于 ImageMagick 图片处理库</td>
		<td ><input class="inp_txt2" size="8" name="settingsnew[watermarktext][skewx]" value="<?php echo $settingsnew['watermarktext'][skewx]?>"  type="text"></td>
	</tr>
	<tr>
		<td ><span>文本水印纵向倾斜角度(ImageMagick):</span>设置水印文本纵向的倾斜角度。本设置只适用于 ImageMagick 图片处理库</td>
		<td ><input class="inp_txt2" size="8" name="settingsnew[watermarktext][skewy]" value="<?php echo $settingsnew['watermarktext'][skewy]?>"  type="text"></td>
	</tr>
</tbody>
	<tr>
		<td colspan="2">
		<input class="inp_btn" name="settingsubmit" value="提 交" type="submit">
		<input class="inp_btn" onclick="
		$('#waterform').attr('action','index.php?admin_setting-preview'); 
		$('#waterform').attr('target','_blank'); 
		$('#waterform').submit();
		$('#waterform').attr('action','index.php?admin_setting-watermark'); 
		$('#waterform').attr('target',''); 
		return false;" 
		value="预览" type="button"> 
		</td>
	</tr>
</table>
</form>
<?php include $this->gettpl('admin_footer');?>