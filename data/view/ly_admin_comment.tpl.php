<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<?php include $this->gettpl('admin_header');?>
<script type="text/javascript" src="js/function.js"></script>
<script type="text/javascript">
function deletecomment(){
	if($("input[name='id[]']:checked").length==0){
		$.dialog.box('immageshow', '注意', '请先选择要删除的内容');
		return false;
	}else{
		if(confirm('确认删除？')){
		$('#formcommentlist').attr("action","index.php?admin_comment-delete");
		$('#formcommentlist').submit();
		}else{
			return false;
		}
	}
}
function selectAll(obj){
	$("input[name='id[]']").attr('checked',obj.checked);
}

function edit_comment(id){
	var comment=$('#c_'+id).html();
	var msg="<textarea id='editcomment' name='editcomment' cols='62' rows='6' >"+comment+"</textarea><br/>"+
	"<input name='editsubmit' class='inp_btn2 m-r10' type='button' onclick='saveedit("+id+")' value='确定'>"+
	"&nbsp;&nbsp;&nbsp;<input name='cancel' type='button' class='inp_btn2 m-r10' onclick='$.dialog.close(\"edit_comment\")' value='取消'>";
	$.dialog.box('edit_comment', '编辑评论', msg);
	//setPopMsg(msg);
	//divDance('popmsg');
	//showPop(465,165);
}
function saveedit(id){
	var comment=$.trim($('#editcomment').val());
	if(comment==""){
//		alert('评论不能为空。');
		$.dialog.box('immageshow', '注意', '评论不能为空。');
		return false;
	}else if(bytes(comment)>200){
//		alert('评论长度最大为200个字符。');
		$.dialog.box('immageshow', '注意', '评论不能为空。');
		return false;
	}
	$.post(
		"<?php echo $setting['seo_prefix']?>comment-edit",
		{id:id,comment:comment},
		function(xml){
			var message=xml.lastChild.firstChild.nodeValue;
			if(message){
				MSG = '评论修改成功！';
				$('#c_'+id).html(comment);
				$('#z_'+id).html(comment);
				//$('z_'+id).innerHTML = comment;
				//alert($('#z_'+id).html());
				//$('z_'+id).innerHTML=message;
			}else{
//				setPopMsg('发生意外错误。');
				MSG = '发生意外错误。';
			}
			$.dialog.close("edit_comment");
			$('#c_'+id).html(comment);
			//$('#z_'+id).val(comment);
			$.dialog.box('immageshow', '注意', MSG);
			////divDance('popmsg');
			//showPop(300,100);
			//setTimeout(hidePop,3000);
		}
	);
}
</script>
<p class="map">内容管理：评论管理</p>
<div class="synonym">
<form name="list" method="POST" action="index.php?admin_comment-search">
		<ul class="col-ul ul_li_sp m-t10">
			<li><span>按分类:</span>
				<select name="qcattype">
					<option value="0" >所有分类</option>
					<?php echo $catstr?>
	    		</select>
			</li>
			<li><span>按词条名:</span>
				<input type="text" class="inp_txt" name="qtitle"  value="<?php echo $qtitle?>" />
			</li>
			<li><span>按作者:</span>
				<input type="text" class="inp_txt" name="qauthor"  value="<?php echo $qauthor?>" />
			</li>
			<li><span>按时间:</span>
				<input name="qstarttime" type="text" class="inp_txt" onclick="showcalendar(event, this);" readonly="readonly" value="<?php echo $qstarttime?>" />
				—
				<input name="qendtime" type="text" class="inp_txt" onclick="showcalendar(event, this);" readonly="readonly"  value="<?php echo $qendtime?>"/>
			</li>
			<li>
				<input name="submit" type="submit" value="搜 索"   class="inp_btn"/>
			</li>
		</ul>
	</form>
	<h3 class="tol_table">[共  <b><?php echo $commentsum?></b> 条评论]</h3>
    <form name="formcommentlist" id="formcommentlist"  method="POST">
        <table class="table">
          <tr>
		  <td style="width:30px;">ID</td>
            <td style="width:40px;">选择</td>
            <td style="width:250px;">评论</td>
            <td style="width:120px;">词条名称</td>
			<td style="width:80px;">发布者</td>
            <td>发布时间</td>
          </tr>
          	<!-- <?php if($commentsum) { ?> -->
			<?php foreach((array)$commentlist as $comment) {?>
          <tr>
		  <td><?php echo $comment['id']?></td>
            <td><input type="checkbox" name="id[]" value="<?php echo $comment['id']?>_<?php echo $comment['did']?>" /></td>
            <td ><a href="javascript:void(0)" id="z_<?php echo $comment['id']?>"  onclick="edit_comment(<?php echo $comment['id']?>);"><?php echo $comment['partcomment']?></a><span id="c_<?php echo $comment['id']?>" style="display:none"><?php echo $comment['comment']?></span></td>
            <td><a target="_blank" href="index.php?doc-view-<?php echo $comment['did']?>" title="<?php echo $comment['title']?>"><?php echo $comment['title']?></a></td>
            <td><a target="_blank" href="index.php?user-space-<?php echo $comment['authorid']?>"  title="<?php echo $comment['author']?>"><?php echo $comment['author']?></a></td>
            <td><?php echo $comment['time']?></td>
          </tr>
          <?php } ?>
			<!-- <?php } else { ?> -->
          <tr>
            <td colspan="6"><?php echo $message?>没有找到任何评论！</td>
          </tr>
          <!-- <?php } ?> -->
		  <tr>
				<td colspan="6">
				<label class="m-r10"><input name="checkbox" type="checkbox" id="chkall" onclick="selectAll(this);">&nbsp;&nbsp;全选</label>
					<input type="button" class="inp_btn2 m-r10" name="casemanage" onClick="deletecomment();" value="删除" />
				</td>
		</tr>
		<tr>
			<td colspan="6"><p class="fenye a-r"> <?php echo $departstr?> </p></td>
		</tr>
        </table>
   </form>
</div>
<?php include $this->gettpl('admin_footer');?>