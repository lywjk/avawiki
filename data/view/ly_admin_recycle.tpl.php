<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<?php include $this->gettpl('admin_header');?>
<script type="text/javascript">
function selectAll(){
	$("input[name='chkdid[]']").attr('checked',$("input[name='checkbox']").attr('checked'));
}
function updatestatus(type){
	if($("input[name='chkdid[]']:checked").length==0){
		$.dialog.box('immageshow', '注意', '请先选择回收站内容');
		return false;
	}else{
		if(type=='recover'){
			if(confirm('确认要恢复回收站内容吗?')==false){
				return false;
			}else{
				document.formdoclist.action='index.php?admin_recycle-recover';
				document.formdoclist.submit();
			}
		}else if(type== 'remove'){
			if(confirm('删除回收站内容不可恢复,确认删除吗?')==false){
				return false;
			}else{
				document.formdoclist.action='index.php?admin_recycle-remove';
				document.formdoclist.submit();
			}
		}
	}
}
function clearrecycle(){
	if(confirm('清空回收站会删除回收站所有内容,确认要清空回收站吗?')==false){
		return false;
	}else{
		window.location='index.php?admin_recycle-clear';
	}
}
</script>
<script type="text/javascript" src="js/calendar.js"></script>
<p class="map">内容管理：回收站</p>
<div class="synonym">
<form name="list" method="POST" action="index.php?admin_recycle-search">
		<ul class="col-ul ul_li_sp m-t10">
			<li><span>关键字:</span>
				<input type="text" class="inp_txt" name="qtitle"  value="<?php if(!empty($qtitle)) { ?><?php echo $qtitle?><?php } ?>" />
			</li>
			<li><span>类型:</span>
				<select name="qtype">
					<option value="0" <?php if(!empty($qtype) && !$qtype) { ?>selected<?php } ?>>全部类型</option>
					 <option value="doc" <?php if(!empty($qtype) && $qtype=='doc') { ?> selected<?php } ?>>&nbsp;>词条</option>
					 <option value="edition" <?php if(!empty($qtype) && $qtype=='edition') { ?>selected<?php } ?>>&nbsp;>版本</option>
					 <option value="category" <?php if(!empty($qtype) && $qtype=='category') { ?>selected<?php } ?>>&nbsp;>分类</option>
					 <option value="user" <?php if(!empty($qtype) && $qtype=='user') { ?>selected<?php } ?>>&nbsp;>用户</option>
					 <option value="gift" <?php if(!empty($qtype) && $qtype=='gift') { ?>selected<?php } ?>>&nbsp;>礼品</option>
					 <option value="attachment" <?php if(!empty($qtype) && $qtype=='attachment') { ?>selected<?php } ?>>&nbsp;>附件</option>
					 <option value="comment" <?php if(!empty($qtype) && $qtype=='comment') { ?>selected<?php } ?>>&nbsp;>评论</option>
				</select>
			</li>
			<li><span>操作人:</span>
				<input type="text" class="inp_txt" name="qauthor" value="<?php if(!empty($qauthor)) { ?><?php echo $qauthor?><?php } ?>" />
			</li>
			<li><span>操作时间:</span>
				<input name="qstarttime" type="text" class="inp_txt" onclick="showcalendar(event, this);" readonly="readonly" value="<?php if(!empty($qstarttime)) { ?><?php echo $qstarttime?><?php } ?>" />
				—
				<input name="qendtime" type="text" class="inp_txt" onclick="showcalendar(event, this);" readonly="readonly"  value="<?php if(!empty($qendtime)) { ?><?php echo $qendtime?><?php } ?>"/>
			</li>
			<li>
				<input name="recyclesubmit" type="submit" value="搜 索"   class="inp_btn"/>
			</li>
		</ul>
	</form>
	
	<!--
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="40px">回收站共 <b><?php echo $count?></b>个内容</td>
		</tr>
	</table>
	-->
	<h3 class="tol_table">[共 <b><?php echo $count?></b> 条内容]</h3>
	<form method="POST" name="formdoclist" >
		<table class="table">
			<tr>
				<td style="width:40px;">选择</td>
				<td style="width:120px;">类型</td>
				<td style="width:240px;">关键字</td>
				<td style="width:80px;">操作人</td>
				<td>操作时间</td>
			</tr>
			<!-- <?php if($count) { ?> -->
			<?php foreach((array)$recyclelist as $recycle) {?>
			<tr>
				<td><input type="checkbox" name="chkdid[]" value="<?php echo $recycle['id']?>" /></td>
				<td><?php echo $recycle['type']?></td>
				<td><?php echo $recycle['keyword']?></td>
				<td><a href="index.php?user-space-<?php echo $recycle['adminid']?>" target="_blank"><?php echo $recycle['admin']?></a></td>
				<td><?php echo $recycle['dateline']?></td>
			</tr>
			<?php } ?>
			<!-- <?php } else { ?> -->
			<tr>
				<td colspan="5">回收站内容为空</td>
			</tr>
			<!-- <?php } ?> -->
			<tr>
				<td colspan="5">
				<label class="m-r10"><input name="checkbox" type="checkbox" id="chkall" onclick="selectAll(this);">&nbsp;&nbsp;全选</label>
					<input type="button" value="恢复" onclick="updatestatus('recover');" class="inp_btn2 m-r10"/>
					<input type="button" value="删除" onclick="updatestatus('remove');" class="inp_btn2 m-r10"/>
					<input type="button" value="清空回收站" onclick="clearrecycle();" class="inp_btn2 m-r10"/>
				</td>
		</tr>
		<tr>
			<td colspan="5"><p class="fenye a-r"> <?php echo $departstr?> </p></td>
		</tr>
		</table>
	</form>
	</div>
<?php include $this->gettpl('admin_footer');?>