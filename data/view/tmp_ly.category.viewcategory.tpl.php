<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<div id="category" class="columns o-v bor-c_dl category" bid="<?php echo $bid?>">
	<?php foreach((array)$data['subcategory'] as $category) {?>
        <dl class="i6-ff p-b10 bor_no">
          <dt><a href="index.php?category-view-<?php echo $category['parent']['cid']?>" ><?php echo $category['parent']['name']?></a></dt>
          <?php if(isset($category['child'])) { ?>
          <?php foreach((array)$category['child'] as $child) {?><dd><a href="index.php?category-view-<?php echo $child['cid']?>"><?php echo $child['name']?></a></dd><?php } ?>
         <?php } ?>
        </dl>
	<?php } ?>
</div>