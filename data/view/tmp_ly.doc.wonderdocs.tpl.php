<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<div id="jcct" class="columns jcct <?php echo $data['config']['style']?>" bid="<?php echo $bid?>">
<h2 class="col-h2">精彩词条</h2>
<a href="index.php?list-focus-3"  class="more">更多>></a>
<?php if(isset($data['fistwonderdoc'])) { ?>
    <dl class="col-dl">
        <dd class="l"><a href="index.php?doc-view-<?php echo $data['fistwonderdoc']['did']?>"  class="a-img"><img title="<?php echo $data['fistwonderdoc']['title']?>" src="<?php echo $data['fistwonderdoc']['image']?>"/></a></dd>
        <dt class="h1 a-c bold"><a href="index.php?doc-view-<?php echo $data['fistwonderdoc']['did']?>" title="<?php echo $data['fistwonderdoc']['title']?>" ><?php echo $data['fistwonderdoc']['shorttitle']?></a></dt>
        <dd><p><?php echo $data['fistwonderdoc']['summary']?>...<a href="index.php?doc-view-<?php echo $data['fistwonderdoc']['did']?>" >阅读全文>></a></p></dd>
    </dl>
    <ul class="col-ul point font-14 link_blue ">
        <?php foreach((array)$data['list'] as $wondoc) {?>
            <li><a href="index.php?doc-view-<?php echo $wondoc['did']?>" ><?php echo $wondoc['title']?></a>: <?php echo $wondoc['summary']?></li>
        <?php } ?>
    </ul>
<?php } ?>
</div>