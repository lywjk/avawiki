<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<?php include $this->gettpl('admin_header');?>
<script type="text/javascript">
function doSubmit(){
	attachment_size=$.trim($('#attachment_size').val());
	if(attachment_size==''||isNaN(attachment_size)==true){
		alert('附件限制大小须为数字');
		return false;
	}
	if(confirm('警告：提交后将覆盖系统当前参数设置，是否继续?')==false){
		return false;
	}
}
</script>
<p class="map">全局：内容设置</p>
<p class="sec_nav">内容设置： <a href="index.php?admin_setting-index"> <span>首页设置</span></a> <a href="index.php?admin_setting-listdisplay"><span>列表设置</span></a> <a href="index.php?admin_setting-watermark"><span>图片设置</span></a> <a href="index.php?admin_setting-docset" ><span>词条设置</span></a> <a href="index.php?admin_setting-search" ><span>搜索设置</span></a> <a href="index.php?admin_setting-attachment" class="on"><span>附件设置</span></a></p>
<h3 class="col-h3">附件设置</h3>
<form method="POST" action="index.php?admin_setting-attachment" onsubmit="return doSubmit();">
	<table class="table">
		<tr>
			<td width="255px;"><span>参数名称</span></td>
			<td><span>参数值</span></td>
		</tr>
		<tr>
			<td >开启附件功能</td>
			<td ><input type="radio" name="attachment_open" value="1" <?php if($setting['attachment_open']==1) { ?>checked<?php } ?> />是&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="attachment_open" value="0" <?php if($setting['attachment_open']!=1) { ?>checked<?php } ?> />否</td>
		</tr>
		<tr>
			<td >附件限制大小<br />(当前PHP允许上传的最大值为: <?php echo $sys_upload_limit?> KB, <br/>当您设置的数字大于此值时，则以此为准！)</td>
			<td ><input class="inp_txt" name="attachment_size" id="attachment_size" value="<?php echo $setting['attachment_size']?>">KB</td>
		</tr>
		<tr>
			<td >附件文件类型<br />类型以 | 分割，使用 * 号表示不限制文件类型</td>
			<td ><textarea class="textarea" name="attachment_type" rows="10"><?php echo $setting['attachment_type']?></textarea></td>
		</tr>
		<tr>
			<td colspan="2"><input class="inp_btn" type="submit" value="保 存" name="attachmentsubmit" /></td>
		</tr>
	</table>
</form>
<?php include $this->gettpl('admin_footer');?>