<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<div id="first">
    <h3>选择要添加的版块</h3>
    <div class="myleft">
        <ul>
        <?php foreach((array)$blocks as $key=>$block) {?>
        <li onclick="block.view('<?php echo $key?>',this)"><a href="javascript:void(0);" onclick="return false"><span><?php echo $block['name']?></span></a></li>
        <?php }?>
        </ul>
    </div>
    <div class='myright'>
    <?php foreach((array)$blocks as $key=>$block) {?>
        <div id="<?php echo $key?>" style="display:none">
        
        <ul>
        <?php foreach((array)$block['fun'] as $funkey=>$fun) {?>
            <li id='<?php echo $key?>-<?php echo $funkey?>' title="block/<?php echo $block['theme_dir']?>/<?php echo $key?>/<?php echo $funkey?>" onclick='block.select(this);'>
            <?php echo $funkey?><br />
            <?php echo $fun?>
            </li> 
        <?php }?>
        </ul>
        
        </div>
    <?php }?>  
    </div>
    <p class="col-p"><input  type="button" class="btn" value="下一步" onclick="block.next()" /></p>
</div>

<div id="second" style="display:none">
    <h3>要添加到本页的区域</h3>
    <div class="main2">
         <select id="area">
         </select>
    </div>
    <br />
    <h3>版块参数设置</h3>
    <form>
    <div id="config" class="main2">
    
    </div>
    </form>
    <p class="col-p m-l140"><input  type="button" class="btn" value="完 成" onclick="block.complete()" /></p>
</div>

  
