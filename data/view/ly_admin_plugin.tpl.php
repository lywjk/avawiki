<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<?php include $this->gettpl('admin_header');?>
<p class="map">插件/模板：插件</p>
<p class="sec_nav">插件：
	<a href="index.php?admin_plugin" class="on"><span>已安装插件</span></a>
    <a href="index.php?admin_plugin-will" ><span>全部推荐插件</span></a>
    <a href="index.php?admin_plugin-find" ><span>本地已有插件</span></a>
</p>
<h3 class="col-h3">已安装插件</h3>
<?php foreach((array)$pluginlist as $plugin) {?>
<div class="plugin_list">
	<?php if(file_exists('plugins/'.$plugin['identifier'].'/screenshot.jpg')) { ?>
		<img src="plugins/<?php echo $plugin['identifier']?>/screenshot.jpg" class="plugin_img"/>
	<?php } else { ?>
		<img src="style/default/plugin.jpg" class="plugin_img"/>
	<?php } ?>
	<dl>
		<dt><a href="index.php?admin_plugin-manage-<?php echo $plugin['pluginid']?>" class="h2"><?php echo $plugin['name']?></a>版本 <?php echo $plugin['version']?></dt>
		<dd>作者:<a href="<?php echo $plugin['homepage']?>" target="_blank"><?php echo $plugin['copyright']?></a></dd>
		<dd class="hei48">描述:<?php echo $plugin['description']?></dd>
	</dl>
	<div class="a-c"> 
		<input type="button" value="分享" onclick="window.location='index.php?admin_plugin-share-<?php echo $plugin['pluginid']?>';" class="btn_plug"/>
		<input type="button" value="管理" onclick="window.location='index.php?admin_plugin-manage-<?php echo $plugin['pluginid']?>'" class="btn_plug"/>
		<?php if($plugin['available']) { ?><input type="button" value="停用" onclick="window.location='index.php?admin_plugin-stop-<?php echo $plugin['pluginid']?>'" class="btn_plug"/><?php } else { ?><input type="button" value="启用" onclick="window.location='index.php?admin_plugin-start-<?php echo $plugin['pluginid']?>'" class="btn_plug"/><?php } ?>
		<input type="button" value="卸载" onclick="window.location='index.php?admin_plugin-uninstall-<?php echo $plugin['pluginid']?>'" class="btn_plug"/>
	</div>
</div>
<?php } ?>

<?php if(empty($pluginlist)) { ?>
没有找到任何插件
<?php } ?>
 
<?php include $this->gettpl('admin_footer');?>