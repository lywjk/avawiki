<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<?php include $this->gettpl('header');?>
<div id="category" class="l w-710 o-v bor-c_dl category">
	<div class="hd_map">
	<a href="<?php echo WIKI_URL?>"><?php echo $setting['site_name']?></a> &gt;&gt;百科分类
	</div>
	<?php if(!empty($subcategory)) { ?>
	<?php foreach((array)$subcategory as $category) {?>
	<dl class="i6-ff m-t10 p-b10">
	  <dt><a href="index.php?category-view-<?php echo $category['parent']['cid']?>" ><?php echo $category['parent']['name']?></a></dt>
	  <?php if(!empty($category['child'])) { ?>
	  <?php foreach((array)$category['child'] as $child) {?><dd><a href="index.php?category-view-<?php echo $child['cid']?>"><?php echo $child['name']?></a></dd><?php } ?>
	 <?php } ?>
	</dl>
	<?php } ?>
	<?php } ?>
</div>
<div class="r w-230">
    <div id="block_right"></div>
	<!--ad start -->
	<div class="ad" id="advlist_7">
	<?php if(isset($advlist[7]) && isset($setting['advmode']) && '1'==$setting['advmode']) { ?>
	<?php echo $advlist[7][code]?>
	<?php } ?>
	</div>
	<!--ad end -->	
</div>
<?php include $this->gettpl('footer');?>