<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo WIKI_CHARSET?>" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title><?php echo $navtitle?> <?php echo $setting['site_name']?> <?php echo $setting['seo_title']?> - Powered by HDWiki!</title>
<?php echo $setting['seo_headers']?>

<meta name="keywords" content="<?php echo $setting['seo_keywords']?>" />
<meta name="description" content="<?php echo $setting['seo_description']?>" />
<meta name="generator" content="HDWiki <?php echo HDWIKI_VERSION?>" />
<meta name="author" content="HDWiki Team and Hudong UI team" />
<meta name="copyright" content="2005-2013 baike.com" />
<?php if(!empty($docrewrite) && $docrewrite=='1') { ?><base href="<?php echo WIKI_URL?>/" /><?php } ?>
<link href="style/<?php echo $theme?>/hdwiki.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="lang/<?php echo $setting['lang_name']?>/front.js"></script>
<script type="text/javascript" src="js/jquery.dialog-0.8.min.js"></script>
<script type="text/javascript" src="js/login.js"></script>

<link type="text/css" rel="stylesheet" href="style/<?php echo $theme?>/materialize/css/materialize.min.css"  media="screen,projection"/>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="style/<?php echo $theme?>/materialize/js/materialize.min.js"></script>

<script type="text/javascript">
$.dialog.setConfig('base', '<?php echo WIKI_URL?>/style/default');
var g_isLogin, g_isUcenter=false, g_forward = '',g_api_url='', g_regulars = '', g_uname_minlength, g_uname_maxlength;
<?php if($user['groupid']=='1') { ?>
g_regulars = "<?php echo $header_regulars?>";
g_isLogin = false;
<?php } else { ?>
g_isLogin = true;
<?php } ?>
<?php if(isset($pp_api)) { ?>
g_api_url = '<?php echo $pp_api?>';
<?php } ?>
<?php if(!empty($isUcenter)) { ?>
g_isUcenter = true;
<?php } ?>
g_seo_prefix = "<?php echo $setting['seo_prefix']?>";
g_seo_suffix = "<?php echo $setting['seo_suffix']?>";
g_uname_minlength = "<?php echo $setting['name_min_length']?>"||3;
g_uname_maxlength = "<?php echo $setting['name_max_length']?>"||15;
<?php if($newpms[0]) { ?>
	var titlestate = 0, clock, flashingtime = 20;
	var oldtitle = "<?php echo $navtitle?> <?php echo $setting['site_name']?> <?php echo $setting['seo_title']?> - Powered by HDWiki!";
	function changeTitle(){
		if(titlestate%2 == 0){
			document.title='【新消息】'+oldtitle;
		}else{
			document.title='【　　　】'+oldtitle;
		}
		titlestate++;
		if(titlestate == flashingtime){
			clearInterval(clock);
			document.title = oldtitle;
		}
	}
	clock = setInterval("changeTitle()", 1000);

<?php } ?>
</script>
</head>
<body>
</ul>
    <!-- 导航 -->
	<nav class="teal">
		<div class="nav-wrapper row">
			<div class="col s12">
				<div class="col s3">
					<a href="<?php echo WIKI_URL?>"  clss="brand-logo" id="logo"><img alt="HDWiki" width="<?php echo $setting['logowidth']?>" height="20px;" src="style/default/logo.gif"/></a>
				</div>
				
				<div class="col s4">

					<?php if(!empty($isimage) ) { ?>
					<form name="searchform" method="post" action="index.php?pic-search">
						<p id="search" class="col s4">
							<input name="searchtext" type="text" class="col s10"  maxlength="80" size="42"  value="<?php if(isset($searchtext)) { ?><?php echo $searchtext?><?php } ?>"/>
							<button class="btn waves-effect waves-light teal col s2" type="submit" name="searchfull" value="图片搜索">
								<i class="mdi-action-search right"></i>
							</button>
						</p>
					</form>
					<?php } else { ?>
					<form name="searchform" method="post" action="index.php?search-kw">
						<p id="search" class="">
							<?php if($cloudsearchhead) { ?>
							<input name="searchtext" class="col s6" maxlength="80" size="42" value="<?php if(isset($searchtext)) { ?><?php echo $searchtext?><?php } ?>" type="text"/>
							<input name="search" value="搜 索" tabindex="1" class=" "  type="submit"/>
							<?php } else { ?>
							<input name="searchtext" class="col s6" maxlength="80" size="42" value="<?php if(isset($searchtext)) { ?><?php echo $searchtext?><?php } ?>" type="text"/>
							<input name="full" value="1" tabindex="1"   type="hidden"/>
							<button class="btn-flat waves-effect waves-light teal col s2" tabindex="2" type="button" name="default" value="进入词条" onclick="document.searchform.action='index.php?search-default';document.searchform.submit();">
								进入词条
							</button>
							<button class="btn-flat waves-effect waves-light teal col s2" tabindex="3" type="submit" name="search" value="搜 索">
								<i class="mdi-action-search"></i>
							</button>
							<a href="index.php?search-fulltext" class="btn-flat waves-effect waves-light teal col s2">高级搜索</a>
							<?php } ?>
						</p>
					</form>
					<?php } ?>

				</div>

				<div class="col s5">
        		<ul class="side-nav">
          			<?php if(!empty($channellist[2])) { ?>
					<?php foreach((array)$channellist[2] as $channel) {?>
					<li><a href="<?php echo $channel['url']?>"><?php echo $channel['name']?></a></li>
					<?php } ?>
					<?php } ?>

					<?php foreach((array)$pluginlist as $plugin) {?>
					<?php if($plugin['type']) { ?>
						<li><a href="index.php?plugin-<?php echo $plugin['identifier']?>"><?php echo $plugin['name']?></a></li>
					<?php } ?>
					<?php } ?>
					<li><a href="index.php?doc-create">创建词条</a></li>
					<li><a href="index.php?doc-sandbox">编辑实验</a></li>
       		 	</ul>
       		 	</div>
      		</div>
    	</div>
   </nav>
	

