<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<div id="login-static" class="columns login-static i-login <?php echo $data['config']['style']?>" bid="<?php echo $bid?>">
	<h2 class="col-h2">登录用户</h2>
	<?php if($user['groupid']=='1') { ?>
		<?php if($data['data']['passport']) { ?>
		<span class="red error" style="top:80px">已开启通行证,请直接点击<a href="index.php?user-login">登录</a></span>
		<?php } else { ?>
		<span class="error" id="logintip"></span>
		<form action="" onsubmit="return docheck();">
		<ul class="col-ul" id="nologin" style="display:block">
			<li><span>用户名：</span><input name="username" id="username" tabindex="3" type="text" class="inp_txt" onblur="check_username()" maxlength="32" /></li>
			<li><span>密  码：</span><input name="password" id="password"  tabindex="4" type="password" class="inp_txt" onblur="check_passwd()" maxlength="32" /></li>
			<?php if($data['data']['checkcode'] != 3) { ?>
			<li class="yzm"><span>验证码：</span><input name="code" id="code"  tabindex="5" type="text" onblur="check_code()" maxlength="4" /><label class="m-lr8"><img id="verifycode" src="index.php?user-code" onclick="updateverifycode();" /></label><a href="javascript:updateverifycode();">换一个</a>
			</li>
			<?php } ?>
			<li><input name="submit" type="submit" value="登录" class="btn_inp" tabindex="6" /><input name="Button1" type="button" value="我要注册" class="btn_inp" onclick="location.href='index.php?user-register';" /></li>
		</ul>
		</form>
		<?php } ?>
	<?php } else { ?>
	<dl id="islogin" class="col-dl twhp" >
	<dd class="block"><a href="index.php?user-space-<?php echo $user['uid']?>" class="a-img1"><img alt="点击进入用户中心" src="<?php if($user['image']) { ?><?php echo $user['image']?><?php } else { ?>style/default/user_l.jpg<?php } ?>" width="36"/></a></dd>
	<dt><a href="index.php?user-space-<?php echo $user['uid']?>" class="m-r8 bold black"><?php echo $user['username']?></a><img title="您现在拥有<?php echo $user['credit1']?>金币 " src="style/default/jb.gif" class="sign"/></dt>
	<dd class="m-b8"><span>头衔：<font color="<?php echo $user['color']?>"><?php echo $user['grouptitle']?></font></span></dd>
	<dd><span>经验：<?php echo $user['credit2']?></span></dd>		
	<dd><span>创建词条：<?php echo $user['creates']?></span><span>人气指数：<?php echo $user['views']?></span></dd>
	<dd class="twhp_dd"><span>编辑词条：<?php echo $user['edits']?></span><a href="index.php?user-space-<?php echo $user['uid']?>" class="red">我的百科</a></dd>
	</dl>
	<?php } ?>
	<p class="novice">
	<a href="index.php?doc-innerlink-<?php echo urlencode('初来乍到，了解一下')?>" >初来乍到，了解一下</a>
	<a href="index.php?doc-innerlink-<?php echo urlencode('我是新手，怎样编写词条')?>" >我是新手，怎样编写词条</a>
	<a href="index.php?doc-innerlink-<?php echo urlencode('我要成为词条达人')?>" >我要成为词条达人</a>
	</p>
<script>
	var indexlogin = 1;
	var loginTip1 = '用户名不能为空!';
	var loginTip2 = "<?php echo $data['data']['loginTip2']?>";
	var loginTip3 = '用户不存在!';
	var logincodewrong = '验证码不匹配!';
	var name_max_length = "<?php echo $data['data']['name_max_length']?>";
	var name_min_length = "<?php echo $data['data']['name_min_length']?>";
	var editPassTip1 = '密码不能为空，最多32位!';
	var loginTip4 = '不匹配!';
	var checkcode = "<?php echo $data['data']['checkcode']?>";
</script>
</div>