<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<?php include $this->gettpl('admin_header');?>
<script type="text/javascript">
function deletecomment(){
	if($("input[name='attach[]']:checked").length==0){
		$.dialog.box('immageshow', '提示', '请选择需要删除的内容');
		return false;
	}else{
		if(confirm('确认删除？')){
			$('#formattachlist').attr("action","index.php?admin_attachment-remove");
			$('#formattachlist').submit();
		}else{
			return false;
		}
	}
}
function selectAll(obj){
	$("input[name='attach[]']").attr('checked',obj.checked);
}

function showimg(){
	$.dialog();
}

$(document).ready(function(){
	$('a[att]').click(function(){
		var url = $(this).attr('att');
		if(url.match(/\.(jpg|gif|png)$/i)){
			$.dialog.box("image", $(this).text(), 'img:'+url);
			return false;
		}
	});
});
</script>
<p class="map">内容管理：附件管理</p>
<div class="synonym">
	<form name="list" method="POST" action="index.php?admin_attachment-search">
		<ul class="col-ul ul_li_sp m-t10">
			<li><span>查找附件:</span>
				<select name="qcattype">
					<option value="0" >所有分类</option>
					<?php echo $catstr?>
				</select>
			</li>
			<li><span>按附件类型:</span>
				<select name="qfiletype">
					<option value="0" >所有类型</option>
					<?php foreach((array)$filetype as $type) {?>
					<option value="<?php echo $type?>" ><?php echo $type?></option>
					<?php } ?>
					<?php echo $filetype?>
				</select>
			</li>
			<li><span>按词条名:</span>
				<input name="qtitle" type="text" class="inp_txt"  value="<?php echo $qtitle?>" />
			</li>
			<li><span>按作者:</span>
				<input type="text" class="inp_txt" name="qauthor" value="<?php echo $qauthor?>" />
			</li>
			<li><span>按时间:</span>
				<input name="qstarttime" type="text" class="inp_txt" onclick="showcalendar(event, this);" readonly="readonly" value="<?php echo $qstarttime?>" />
				—
				<input name="qendtime" type="text" class="inp_txt" onclick="showcalendar(event, this);" readonly="readonly"  value="<?php echo $qendtime?>"/>
			</li>
			<li>
				<input name="submit" type="submit" value="搜 索"   class="inp_btn"/>
			</li>
		</ul>
	</form>
	<h3 class="tol_table">[共 <b><?php echo $attachsum?></b> 个附件]</h3>
	<form name="formattachlist" id="formattachlist"  method="POST">
		<table class="table">
			<tr>
				<td style="width:30px;">选择</td>
				<td style="width:180px;">附件名称</td>
				<td style="width:120px;">词条名称</td>
				<td style="width:60px;">附件大小</td>
				<td style="width:50px;">发布者</td>
				<td style="width:100px;">上传时间</td>
				<td style="width:60px;">下载次数</td>
				<td >描述</td>
			</tr>
			<!-- <?php if($attachlist != null) { ?> -->
			<?php foreach((array)$attachlist as $attach) {?>
			<tr>
				<td><input type="checkbox" class="box" name="attach[]" value="<?php echo $attach['id']?>_<?php echo $attach['attachment']?>"  /></td>
				<td ><a href="index.php?admin_attachment-download-<?php echo $attach['id']?>-<?php echo $attach['isimage']?>" att="<?php echo $attach['attachment']?>"><?php echo $attach['filename']?></a></td>
				<td><a target="_blank"  href="index.php?doc-view-<?php echo $attach['did']?>" title="<?php echo $attach['title']?>"><?php echo $attach['title']?></a></td>
				<td><?php echo $attach['filesize']?></td>
				<td><a target="_blank" href="index.php?user-space-<?php echo $attach['uid']?>"  title="<?php echo $attach['author']?>"><?php echo $attach['author']?></a></td>
				<td><?php echo $attach['time']?></td>
				<td><?php echo $attach['downloads']?></td>
				<td><?php echo $attach['description']?></td>
			</tr>
			<?php } ?>
			<!-- <?php } else { ?> -->
			<tr>
				<td colspan="8"><?php echo $message?>没有找到任何附件！</td>
			</tr>
			<!-- <?php } ?> -->
			<tr>
				<td colspan="8">
				<label class="m-r10"><input name="checkbox" type="checkbox" id="chkall" onclick="selectAll(this);">&nbsp;&nbsp;全选</label>
					<input type="button" class="inp_btn2 m-r10" name="casemanage" onClick="deletecomment();" value="删除" />
				</td>
			</tr>
			<tr>
				<td colspan="8"><p class="fenye a-r"> <?php echo $departstr?> </p></td>
			</tr>
		</table>
	</form>
</div>
<?php include $this->gettpl('admin_footer');?>