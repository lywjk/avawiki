<?php if(!defined('HDWIKI_ROOT')) exit('Access Denied');?>
<?php include $this->gettpl('admin_header');?>
<style type="text/css">
.select{ border:1px #ccc solid; color:blank;}
.black{background-color:#333; color:white;}
.white{ background-color:white;}
.pink{ background-color:pink;}
.blue{ background-color:blue; color:white;}
.green{ background-color:green; color:white;}
.red{ background-color:red;color:white;}
.brown{ background-color:brown; color:white;}
.orange{ background-color:orange; }
.gray{ background-color:gray; color:white;}
.purple{ background-color:purple; color:white;}
</style>
<script language="JavaScript" type="text/javascript">
function IsURL(str_url){
	return(/(?:https?:\/\/)?\w+\.\w+/.test(str_url));
}
function checknull(){

	if($.trim($("#path").val()).length>20){
		alert('目录名称不能超过20个字');
		$('#path').focus();
		return false;
	}else if($.trim($("#name").val()).length>20){
		alert('模版名称不能超过20个字');
		$('#name').focus();
		return false;
	}else if($.trim($("#author").val()).length>10){
		alert('作者不能超过10个字');
		$('#author').focus();
		return false;
	}else if($.trim($("#desc").val()).length>150){
		alert('描述不能超过150个字');
		$('#desc').focus();
		return false;
	}else if($.trim($("#sitename").val()).length>30){
		alert('支持网站名称不能超过30个字');
		$('#sitename').focus();
		return false;
	}else if($.trim($("#version").val()).length>15){
		alert('模版版本不能超过15个字');
		$('#version').focus();
		return false;
	}else if($.trim($("#copyright").val()).length>50){
		alert('版权不能超过50个字');
		$('#copyright').focus();
		return false;
	}else{
		if($.trim($("#path").val())==""){
			alert("目录名称不能为空。");
			$('#path').focus();
			return false;
		}else if($.trim($("#name").val())==""){
			alert("名称不能为空。");
			$('#name').focus();
			return false;
		}else if($.trim($("#tag").val())==""){
			alert("标签不能为空。");
			$('#tag').focus();
			return false;
		}else if($.trim($("#author").val())==""){
			alert("作者名称不能为空。");
			$('#author').focus();
			return false;
		}else if(IsURL($.trim($('#authorurl').val()))==false){
			alert("作者链接地址格式不正确，请重新填写。");
			$('#authorurl').focus();
			return false;
		}else if($.trim($("#desc").val())==""){
			alert("描述不能为空。");
			$('#desc').focus();
			return false;
		}else if($.trim($("#sitename").val())==""){
			alert("网站名称不能为空。");
			$('#sitename').focus();
			return false;
		}else if(IsURL($.trim($('#weburl').val()))==false){
			alert("网站地址格式不正确，请重新填写。");
			$('#weburl').focus();
			return false;
		}else if($.trim($("#version").val())==""){
			alert("版本不能为空。");
			$('#version').focus();
			return false;
		}else if($.trim($("#copyright").val())==""){
			alert("版权不能为空。");
			$('#copyright').focus();
			return false;
		}
	}
}
function selectcolor(){
	$("#tag").val($("#tag").val()+' '+$.trim($("#stylecolor").val()));
}
function selectlayout(){
	$("#tag").val($("#tag").val()+' '+$.trim($("#stylelayout").val()));
}
</script>
        
<p class="map">插件/模板：创建模版</p>
<p class="sec_nav">模板：
    <a href="index.php?admin_theme"><span>设置默认风格</span></a>
    <a href="index.php?admin_theme-create" class="on"><span>创建风格</span></a>
    <a href="index.php?admin_theme-list"><span>在线安装</span></a>
    <a href="index.php?admin_theme-edit" ><span>模板编辑</span></a>
</p>
        
<h3 class="col-h3">创建模板</h3><br />

        
		<form name="_modifyico" enctype="multipart/form-data" action="index.php?admin_theme-createstyle" method="POST" onsubmit="return checknull();">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="mix_eidt">
				<thead>
				<tr>
					<td colspan="2">
				  		<?php if($defaultstyle['name']!='' && $defaultstyle['img']!='1') { ?>
								<img alt="<?php echo $defaultstyle['name']?>" src="style/default/screenshot.jpg"/>
						<?php } else { ?>
								<img alt="<?php echo $defaultstyle['name']?>" src="style/<?php echo $defaultstyle['path']?>/screenshot.jpg"/>
				  		<?php } ?>
					</td>
				</tr>
				</thead>
				<tr>
					<td >&nbsp;&nbsp;目录名称:</td>
					<td ><input style="width:200px;" class="inp_txt" id="path" name="style[path]" type="text" value="" /><font color="red">(此目录为view下唯一标识，请务必修改成英文名称!)</font></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;模版名称:</td>
					<td ><input style="width:200px;" class="inp_txt" name="style[name]" id="name" type="text" value="" /></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;标签:</td>
					<td >
					<select name="stylecolor" id="stylecolor" class="select" onchange="selectcolor();">
						<option value="" selected="selected">选择色系</option>
						<option class="black" value="黑色">黑色</option>
						<option class="white" value="白色">白色</option>
						<option class="pink" value="粉色">粉色</option>
						<option class="blue" value="蓝色">蓝色</option>
						<option class="green" value="绿色">绿色</option>
						<option class="red" value="红色">红色</option>
						<option class="brown" value="棕色">棕色</option>
						<option class="gray" value="灰色">灰色</option>
						<option class="orange" value="黄色">黄色</option>
						<option class="purple" value="紫色">紫色</option>
						<option class="white" value="其他色">其他色</option>
					</select>
					<select name="stylelayout" id="stylelayout" onchange="selectlayout();">
						<option value="" selected="selected">选择版式</option>
						<option value="一栏布局">一栏布局</option>
						<option value="二栏布局">二栏布局</option>
						<option value="三栏布局">三栏布局</option>
						<option value="其它布局">其它布局</option>
					</select>    
					<input style="width:300px;" class="inp_txt" name="style[tag]" id="tag" type="text" value="" />
					<font color="red">(多个标签用空格隔开)</font></td>
				</tr>
				<tr>
					<td style="width:180px;">&nbsp;&nbsp;作者:</td>
					<td ><input style="width:200px;" class="inp_txt" id="author" name="style[author]" type="text" value="" /></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;作者链接:</td>
					<td ><input style="width:200px;" class="inp_txt" id="authorurl" name="style[authorurl]" type="text" value="http://"  /><font color="red">(请填写您的个人主页或blog，以便使用者与作者进行交流)</font></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;描述:</td>
					<td ><textarea name="style[desc]" id="desc" style="width:300px;" rows="4" cols="47"></textarea>
					</td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;支持网站名称:</td>
					<td ><input style="width:200px;" class="inp_txt" name="style[sitename]" id="sitename" type="text" value="" /></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;使用说明地址:</td>
					<td ><input style="width:200px;" class="inp_txt" name="style[weburl]" id="weburl" type="text" value="http://" /></td>
				</tr>
				<tr>
					<td >&nbsp;&nbsp;模版版本:</td>
					<td ><input style="width:200px;" class="inp_txt" name="style[version]" id="version" type="text" value="" /></td>
				</tr>				
				<tr>
					<td >&nbsp;&nbsp;版权:</td>
					<td ><input style="width:200px;" class="inp_txt" name="style[copyright]" id="copyright" type="text" value="" /></td>
				</tr>
				<tr>
					<td height="40" colspan="2">&nbsp;&nbsp;<input class="inp_btn2" type="submit" value="创建风格" name="stylesubmit" id="stylesubmit" /></td>
				</tr>
			</table>
		</form>
<?php include $this->gettpl('admin_footer');?>