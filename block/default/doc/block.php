<?php
$block['name']='词条相关';
$block['description']    = '本版块包含对于所有和词条相关的调用。';
$block['author']='ly';
$block['version']='5.1';
$block['time']='2015-1-12';
$block['fun'] = array(
	'hotdocs'=>'热门词条',
	'recentdocs'=>'最近更新',
	'commenddocs'=>'推荐词条',
	'wonderdocs'=>'精彩词条',
	'cooperatedocs'=>'待完善词条',
	'hottags'=>'热门标签。',
	'categorydocs'=>'分类下词条',
	'getletterdocs'=>'按字母浏览词条',
	'hotcommentdocs'=>'热评词条'
);
?>